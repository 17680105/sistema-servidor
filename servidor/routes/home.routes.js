const express = require('express');
const router = express.Router();

const personalController = require('../controllers/personal.controller');
const homeController = require('../controllers/home.controller');

router.post('/', personalController.registrarPersonal);
router.get('/', homeController.datosGenerales);

module.exports = router;