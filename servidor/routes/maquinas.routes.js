const express = require('express');
const router = express.Router();

const maquinasController = require('../controllers/maquinas.controller');

router.post('/:tipo', maquinasController.registrarMaquina);
router.get('/:tipo', maquinasController.listarMaquinas);
router.get('/:tipo/:id', maquinasController.obtenerMaquina);
router.put('/:tipo/:id', maquinasController.actualizarMaquina);
router.delete('/:tipo/:id', maquinasController.eliminarMaquina);

module.exports = router;