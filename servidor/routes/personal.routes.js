const express = require('express');
const router = express.Router();

const personalController = require('../controllers/personal.controller');

// Rutas para el personal
//router.post('/', personalController.registrarPersonal);
router.get('/', personalController.listarPersonal);
router.get('/:id', personalController.obtenerPersonal);
router.put('/:id', personalController.actualizarPersonal);
router.delete('/:id', personalController.eliminarPersonal);

module.exports = router;