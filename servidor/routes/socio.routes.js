const express = require('express');
const router = express.Router();

const socioController = require('../controllers/socio.controller');

// Rutas referentes a socios del negocio (personas con membresia de gimnasio)
router.post('/', socioController.registrarSocio);
router.get('/', socioController.listarSocios);
router.get('/:id', socioController.obtenerSocio);
router.put('/:id', socioController.actualizarSocio);
router.delete('/:id', socioController.eliminarSocio);

//router.get('/socios-activos',socioController.sociosActivos); //No usado en el Front-end


module.exports = router;