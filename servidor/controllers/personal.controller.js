const Personal = require("../models/personal.model");

exports.registrarPersonal = async (req, res) => {
    try {
        let personal;
        personal = new Personal(req.body);
        
        await personal.save();
        res.send(personal);
        
    } catch (error) {
        console.log(error);
        res.status(500).send("Ha ocurrido un error");
    }
}

exports.listarPersonal = async (req, res) => {
    try {
        const personal = await Personal.find();
        res.json(personal);
    } catch (error) {
        console.log(error);
        res.status(500).send("Ha ocurrido un error");
    }
}

exports.actualizarPersonal = async (req, res) => {
    try {
        //Ingresados por el usuario
        const { nombre, domicilio, telefono, sueldo, rol, fechaIngreso } = req.body;
        //Busca coincidencia en la base de datos
        let personal = await Personal.findById(req.params.id);

        if(!personal){
            res.status(404).json({msg: "Personal no encontrado"});
        }

        //Actualiza los valores 
        personal.nombre = nombre;
        personal.domicilio = domicilio;
        personal.telefono = telefono;
        personal.sueldo = sueldo;
        personal.rol = rol;
        personal.fechaIngreso = fechaIngreso;

        personal = await Personal.findByIdAndUpdate({_id: req.params.id}, personal, {new: true});
        res.json(personal);
    } catch (error) {
        console.log(error);
        res.status(500).send("Ha ocurrido un error");
    }
}

exports.obtenerPersonal = async (req, res) => {
    // Obtiene los datos de una sola persona
    try {
       
        let personal = await Personal.findById(req.params.id);

        if(!personal){
            res.status(404).json({msg: "Personal no encontrado"});
        }

        res.json(personal);
    } catch (error) {
        console.log(error);
        res.status(500).send("Ha ocurrido un error");
    }
}

exports.eliminarPersonal = async (req, res) => {
    try {
       
        let personal = await Personal.findById(req.params.id);

        if(!personal){
            res.status(404).json({msg: "Personal no encontrado"});
        }

        await Personal.findOneAndRemove({_id: req.params.id});

        res.json({msg: "Personal eliminado exitosamente"});

    } catch (error) {
        console.log(error);
        res.status(500).send("Ha ocurrido un error");
    }
}