const Maquina = require("../models/maquinas.model");

exports.registrarMaquina = async (req, res) => {
    try {
        let maquina = new Maquina(req.body);
        
        await maquina.save();
        res.send(maquina);
        console.log("Agregado correctamente");
        
    } catch (error) {
        console.log(error);
        res.status(500).send("Ha ocurrido un error");
    }
}

exports.listarMaquinas = async (req, res) => {
    try {
        
        const maquina = await Maquina.find({"tipo_maquina": req.params.tipo});
        res.json(maquina);
    } catch (error) {
        console.log(error);
        res.status(500).send("Ha ocurrido un error");
    }
}

exports.actualizarMaquina = async (req, res) => {
    try {
        //Ingresados por el usuario
        const { numero_serie, sector_colocacion, fecha_registro, status } = req.body;
        //Busca coincidencia en la base de datos
        let maquina = await Maquina.findById(req.params.id);

        if(!maquina){
            res.status(404).json({msg: "Máquina no encontrada"});
        }

        //Actualiza los valores 
        maquina.numero_serie = numero_serie;
        maquina.sector_colocacion = sector_colocacion;
        maquina.fecha_registro = fecha_registro;
        if(req.params.tipo == "discos"){
            maquina.descripcion = req.body.descripcion;
        }
        maquina.status = status;

        maquina = await Maquina.findByIdAndUpdate({_id: req.params.id}, maquina, {new: true});
        res.json(maquina);
    } catch (error) {
        console.log(error);
        res.status(500).send("Ha ocurrido un error");
    }
}

exports.obtenerMaquina = async (req, res) => {
    // Obtiene los datos de una sola persona
    try {
       
        let maquina = await Maquina.findById(req.params.id);

        if(!maquina){
            res.status(404).json({msg: "Personal no encontrado"});
        }

        res.json(maquina);
    } catch (error) {
        console.log(error);
        res.status(500).send("Ha ocurrido un error");
    }
}

exports.eliminarMaquina = async (req, res) => {
    try {
       
        let maquina = await Maquina.findById(req.params.id);

        if(!maquina){
            res.status(404).json({msg: "Máquina no encontrada"});
        }

        await Maquina.findOneAndRemove({_id: req.params.id});

        res.json({msg: "Máquina eliminada exitosamente"});

    } catch (error) {
        console.log(error);
        res.status(500).send("Ha ocurrido un error");
    }
}