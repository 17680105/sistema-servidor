const Socio = require("../models/socio.model");
const Ventas = require('../models/ventas.model');

let fecha = new Date(Date.now());
let dia = fecha.getDate();
let mes = fecha.getMonth()
let anio = fecha.getFullYear();
// Marca las 00:00 h del día actual
let condicionFecha = new Date(anio, mes, (dia -1 ), 0, 0, 0);

// Muestra los datos generales del dashboard 
exports.datosGenerales = async (req, res) => {
    try {
        let datos = [];
        let total_venta = 0; //Representa el costo total de la venta realizada
        let consulta;
        let num_ventas = await Ventas.find({"fecha_de_venta": {$gt: condicionFecha}}).countDocuments();
        
        datos.push(await Socio.find({"estado": "Activo"}).countDocuments());
        datos.push(await Socio.countDocuments());
        datos.push(num_ventas);
        // Obtener ventas totales
        consulta = await Ventas.find({"fecha_de_venta": {$gt: condicionFecha}});

        for (const iterator of consulta) {
            total_venta = iterator.total_venta + total_venta;
        }

        datos.push(total_venta);

        res.json(datos);
    } catch (error) {
        console.log(error);
        res.status(500).send("Ha ocurrido un error"); 
    }
}

