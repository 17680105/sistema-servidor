const Ventas = require("../models/ventas.model");

exports.registrarVenta = async (req, res) => {
    try {
        let venta;
        
        venta = new Ventas(req.body);

        await venta.save();
        res.send(venta);
        
    } catch (error) {
        console.log(error);
        res.status(500).send("Ha ocurrido un error");
    }
}

exports.mostrarVentas = async (req, res) => {
    try {
        const ventas = await Ventas.find();
        res.json(ventas);
    } catch (error) {
        console.log(error);
        res.status(500).send("Ha ocurrido un error");
    }
}

// Las ventas realizadas no se pueden alterar
// No se usa
exports.actualizarProducto = async (req, res) => {
    try {
        //Ingresados por el usuario
        const { id_Usuario, Genero_Articulo, Cantidad_Producto,Descripcion_Producto, Precio_Unitario, Total_Articulo } = req.body;
        //Busca coincidencia en la base de datos
        let producto = await Producto.findById(req.params.id);

        if(!producto){
            res.status(404).json({msg: "Producto no encontrado"});
        }

        //Actualiza los valores 
        producto.id_Usuario = id_Usuario;
        producto.Genero_Articulo = Genero_Articulo;
        producto.Descripcion_Producto = Descripcion_Producto;
        producto.Cantidad_Producto = Cantidad_Producto;
        producto.Precio_Unitario = Precio_Unitario;
        producto.Total_Articulo = Total_Articulo;

        producto = await Producto.findByIdAndUpdate({_id: req.params.id}, producto, {new: true});
        res.json(producto);
    } catch (error) {
        console.log(error);
        res.status(500).send("Ha ocurrido un error");
    }
}

// No se usa
exports.obtenerProducto = async (req, res) => {
    try {
       
        let producto = await Producto.findById(req.params.id);

        if(!producto){
            res.status(404).json({msg: "Producto no encontrado"});
        }

        res.json(producto);
    } catch (error) {
        console.log(error);
        res.status(500).send("Ha ocurrido un error");
    }
}

// No se usa
exports.eliminarProducto = async (req, res) => {
    try {
       
        let producto = await Producto.findById(req.params.id);

        if(!producto){
            res.status(404).json({msg: "Producto no encontrado"});
        }

        await Producto.findOneAndRemove({_id: req.params.id});

        res.json({msg: "Producto eliminado exitosamente"});

    } catch (error) {
        console.log(error);
        res.status(500).send("Ha ocurrido un error");
    }
}