﻿const Socio = require("../models/socio.model");
const Counter = require('../models/counter.model');

exports.registrarSocio = async (req, res) => {
    try {
        //Generar el número de socio (autoincremental)
        let count = await Counter.findById({"_id": "socioid"});

        if(!count){
            res.status(404).json({msg: "Secuancia no encontrada"});
        }

        num_socio = await Counter.findByIdAndUpdate({_id: "socioid"}, {$inc:{seq: 1}}, {new: true});

        // Asignación de valores al socio
        let socio;
        socio = new Socio(req.body);
        socio.num_socio = num_socio.seq;

        await socio.save();
        res.send(socio);
        
    } catch (error) {
        console.log(error);
        res.status(500).send("Ha ocurrido un error");
    }
}

exports.listarSocios = async (req, res) => {
    try {
        const socio = await Socio.find();
        res.json(socio);
    } catch (error) {
        console.log(error);
        res.status(500).send("Ha ocurrido un error");
    }
}

exports.actualizarSocio = async (req, res) => {
    try {
        //Ingresados por el usuario
        const { num_socio, nombre_socio, domicilio, telefono, tel_emergencia, ultimo_pago, estado } = req.body;
        //Busca coincidencia en la base de datos
        let socio = await Socio.findById(req.params.id);

        if(!socio){
            res.status(404).json({msg: "Socio no encontrado"});
        }

        //Actualiza los valores 
        socio.num_socio = num_socio;
        socio.nombre_socio = nombre_socio;
        socio.domicilio = domicilio;
        socio.telefono = telefono;
        socio.tel_emergencia = tel_emergencia;
        socio.ultimo_pago = ultimo_pago;
        socio.estado = estado;

        socio = await Socio.findByIdAndUpdate({_id: req.params.id}, socio, {new: true});
        res.json(socio);
    } catch (error) {
        console.log(error);
        res.status(500).send("Ha ocurrido un error");
    }
}

exports.obtenerSocio = async (req, res) => {
    try {
       
        let socio = await Socio.findById(req.params.id);

        if(!socio){
            res.status(404).json({msg: "Socio no encontrado"});
        }

        res.json(socio);
    } catch (error) {
        console.log(error);
        res.status(500).send("Ha ocurrido un error");
    }
}

exports.eliminarSocio = async (req, res) => {
    try {
       
        let socio = await Socio.findById(req.params.id);

        if(!socio){
            res.status(404).json({msg: "Socio no encontrado"});
        }

        await Socio.findOneAndRemove({_id: req.params.id});

        res.json({msg: "Socio eliminado exitosamente"});

    } catch (error) {
        console.log(error);
        res.status(500).send("Ha ocurrido un error");
    }
}

//Muestra los socios activos | NO USADO POR EL MOMENTO
exports.sociosActivos = async (req, res) => {
    try {
        let numSocios = await Socio.find({"estado": "activo"});
        res.json(numSocios);
    } catch (error) {
        console.log(error);
        res.status(500).send("Ha ocurrido un error"); 
    }
}