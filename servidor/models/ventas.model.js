const mongoose = require('mongoose');


const VentasSchema = mongoose.Schema({
    id_usuario: {
        type: String,
        require: true
    },
    items: {
        type: Array,
        require: true
        /*
        Esta parte debe contener lo siguiente (una o más veces)
        con el siguiente formato:
        
        detalle_item01: {
            genero_item: 
            descripcion_item: 
            cantidad_item: 
            precio_unitario:
            precio_total:
        }
        */
    },
    total_venta: {
      type: Number,
      require: true  
    },
    fecha_de_venta: {
        type: Date,
        default: Date.now()
    }
});

module.exports = mongoose.model('ventas', VentasSchema);