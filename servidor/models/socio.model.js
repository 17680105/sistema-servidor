const mongoose = require('mongoose');

const SocioSchema = mongoose.Schema({ // la ruta del schema solo se cambio el nombre
    num_socio: {
        type: Number
    },
    nombre_socio: {
        type: String,
        required: true
    },
    domicilio: {
        type: String,
    },
    telefono: { // el telefono puede variar en number o string
        type: String,
        required: true
    },
    tel_emergencia: {
        type: String
    },
    ultimo_pago: {
        type: String,
        required: true
    },
    estado: { //activo o inactivo | Al registrarse no es necesario tener estado
        type: String,
        default: "Activo"
    },
    tipo_suscripcion: {
        type: String,
        required: true
    },
    fechaCreacion: {
        type: Date,
        default: Date.now()
    }
});


module.exports = mongoose.model('Socio', SocioSchema);