const mongoose = require('mongoose');

const PersonalSchema = mongoose.Schema({ 
    nombre: {
        type: String,
        required: true
    },
    telefono: {
        type: String,
        required: true
    },
    domicilio: {
        type: String,
        required: true
    },
    puesto: {
        type: String,
        required: true
    },
    turno: {
        type: String,
        required: true
    },
    fecha_ingreso: {
        type: Date,
        default: Date.now()
    }
});


module.exports = mongoose.model('Personal', PersonalSchema);