const mongoose = require('mongoose');

const maquinasSchema = mongoose.Schema({
    numero_serie: {
        type: String,
        required: true
    },
    tipo_maquina: {
        //Dependiendo del tipo de máquina se muestran los demás datos
        type: String,
        required: true
    },
    sector_colocacion: {
        type: String,
        required: true
    },
    fecha_registro: {
        type: String,
        required: true
    },
    descripcion:{
        type: String,
        defualt: ""
    },
    status: {
        type: String,
        required: true
    }
});


module.exports = mongoose.model('Maquinas', maquinasSchema);