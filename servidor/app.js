require('./config/config');
require('./models/db');
require('./config/passportConfig');

const express = require('express');
const bodyParser = require('body-parser');
const cors = require('cors');
const routerIntex = require('./routes/routes');
const routerSocios = require('./routes/socio.routes');
const routerPersonal = require('./routes/personal.routes');
const routerVentas = require('./routes/ventas.routes');
const routerHome = require('./routes/home.routes');
const routerMaquinas = require('./routes/maquinas.routes');
const passport = require('passport');

const app = express();

// Middlewares

app.use(bodyParser.json());
app.use(cors());

// Passport Middleware
app.use(passport.initialize());
//app.use(passport.session());

// Al acceder a la ruta /api se inicia el proceso de routerIndex
app.use('/api', routerIntex);
app.use('/api/home', routerHome);
app.use('/api/socios', routerSocios);
app.use('/api/personal', routerPersonal);
app.use('/api/ventas', routerVentas);
app.use('/api/maquinas', routerMaquinas);


// Manejador de errores
app.use((err, req, res, next) => {
    if (err.name === 'ValidationError') {
        var valErrors = [];
        Object.keys(err.errors).forEach(key => valErrors.push(err.errors[key].message));
        res.status(422).send(valErrors);
    }
});

app.listen(process.env.PORT, () => {
    console.log(`Servidor iniciado en el puerto ${process.env.PORT}`);
});